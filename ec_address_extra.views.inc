<?php

/**
 * Implementation of hook_views_data().
 */
function ec_address_extra_views_data() {
  $data = array();
  
  $data['ec_transaction_address']['phone'] = array(
    'title' => t('Phone'),
    'help' => t('The phone number for the customer'),
    'field' => array(
      'field' => 'phone',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  
  if (module_exists('ec_address')) {
    $data['ec_address']['phone'] = $data['ec_transaction_address']['phone'];
  }
  
  return $data;
}

This module is mainly provided as an example of how to add additional lines of
fields to addresses within the e-Commerce checkout without having to alter the
core of e-Commerce

Right now it only adds a phone field but more can be added.

Also I hope that this module will be depreciated in Drupal 7 and using the
Field API to add additional fields will be the required method.